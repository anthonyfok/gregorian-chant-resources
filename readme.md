##Gregorian Chant Resources 

This is a project to share all the music I've collected for singing Gregorian chant for the Tridentine Mass. Most of the music comes from a now defunct “quilisma-publications.info” website[^1] which had a *Liber Usualis* in modern notation. I've also got the Rossini propers which are in modern notation but very simple. Good for choirs that are just starting.

I have tried to pull out each Sunday and holy day into its own PDF or a small number of PDFs for easy printing, but this is still kind of incomplete. All the music is here however; it's just that you might have to find it in the PDFs of the original Quilisma Publications files.

A note of warning, the *Liber Usualis* on the Quilisma Publications website is from 1908 or something. The 1962 Missal differs a tiny bit throughout the year, but is very different at Holy Week. I have not yet found good modern notation resources for Holy Week; feel free to let me know if you find one. (or make one!)

[^1]: Apparently, the [Quilisma Publications—Repository of Liturgical Music](http://www.lakewoodsound.com/quilisma/) website now resides at http://www.lakewoodsound.com/quilisma/ .
